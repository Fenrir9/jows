#!/usr/bin/env python

from mininet.net import Mininet
from mininet.log import setLogLevel, info
from mininet.link import TCLink

import logging 
import threading
import time

class ClinicLANSimulation():

    threads = list()

    def __init__( self ):
        self.net = Mininet( autoStaticArp=True, waitConnected=True )

        self.configureTopology()
        self.setupLinks()
        self.testAllConnections()
        self.configureBandwidth()
        self.configurePacketLoss()
        self.configureDelays()
        self.runBandwidthTests()
        self.runPingCollectMeasurements()
        self.stopTesting()

    def configureTopology( self ):
        self.net.addController( 'c0' )
        self.receptionComputer = self.net.addHost( 'h1' )
        self.receptionVoIP = self.net.addHost( 'h2' )
        self.doctor1Computer = self.net.addHost( 'h3' )
        self.doctor1VoIP = self.net.addHost( 'h4' )
        self.doctor2Computer = self.net.addHost( 'h5' )
        self.doctor2VoIP = self.net.addHost( 'h6' )
        self.firstFloorSwitch = self.net.addSwitch( 's1' )
        self.secondFloorSwitch = self.net.addSwitch( 's2' )

    def setupLinks( self ):
        self.link1 = self.net.addLink( self.receptionComputer, self.firstFloorSwitch, cls=TCLink )
        self.net.addLink( self.receptionVoIP, self.firstFloorSwitch )
        self.link2 = self.net.addLink( self.doctor1Computer, self.secondFloorSwitch, cls=TCLink )
        self.net.addLink( self.doctor1VoIP, self.secondFloorSwitch )
        self.link3 = self.net.addLink( self.doctor2Computer, self.secondFloorSwitch, cls=TCLink )
        self.net.addLink( self.doctor2VoIP, self.secondFloorSwitch )
        self.net.addLink( self.firstFloorSwitch, self.secondFloorSwitch )
        self.net.start()
    
    def testAllConnections( self ):
        self.net.pingAll()
    
    def configureBandwidth( self ):
        info( '\n*** Configuring intfs bandwidth' )
        self.link1.intf1.config( bw=100 )
        self.link2.intf1.config( bw=100 )
        self.link3.intf1.config( bw=100 )

    def parallel_test( self, source, destination, protocol, time, information):
        self.net.iperf( ( source, destination ), l4Type=protocol,  seconds=time )
        info( information )

    def runBandwidthTests( self ):
        info( '\n*** Running iperf to test\n' )
        test1 = threading.Thread( target=self.parallel_test, args=( self.receptionComputer, self.doctor1Computer, 'TCP', 10, "test between receptionCompter and doctor1Computer"))
        self.threads.append(test1)
        test1.start()
        test2 = threading.Thread( target=self.parallel_test, args=( self.receptionVoIP, self.doctor1VoIP, 'UDP', 10, "test between receptionVoIP and doctor1VoIP"))
        self.threads.append(test2)
        test2.start()
        test1.join()
        test2.join()

    def configurePacketLoss( self ):
        info( '\n*** Configuring intfs loss of 5%\n' )
        self.link1.intf1.config( loss=5 )
        self.link2.intf1.config( loss=5 )
        self.link3.intf1.config( loss=5 )
        info( '\n' )

    def configureDelays( self ):
        info( '\n*** Configuring one intfs delay of 15ms\n' )
        self.link1.intf1.config( delay='15ms' )
        self.link2.intf1.config( delay='15ms' )
        self.link3.intf1.config( delay='15ms' )

    def runPingCollectMeasurements( self ):
        info( '\n*** Run a ping to confirm delay\n' )
        self.net.pingPairFull()

    def stopTesting( self ):
        info( '\n*** Done testing\n' )
        self.net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    clinicLANSimulation = ClinicLANSimulation()
